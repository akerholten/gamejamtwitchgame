﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
    idle = 0,
    walking = 1,
    attack = 2
}

public class CharacterAnimator : MonoBehaviour {

    private SpriteRenderer spriteRenderer;
    private Animator animator;

	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
	}
	
	void Update () {
		
	}

    public void UpdatePlayerState(PlayerState playerState)
    {
        animator.SetInteger("playerState", (int)playerState);
    }
}
