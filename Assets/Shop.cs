﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Shop Type", menuName = "Shop Type")]
public class Shop : ScriptableObject {

    // Upgrades themselves could be structs or scriptable objects

    public int healthModifierCost       = 100;
    public int atkSpeedModifierCost     = 150;
    public int atkRangeModifierCost     = 250;
    public int atkDamageModifierCost    = 500;

    public int healthMaxLvl     = 20;
    public int atkSpeedMaxLvl   = 20;
    public int atkRangeMaxLvl   = 20;
    public int atkDamageMaxLvl  = 20;

    public float healthModifier     = 0.1f;
    public float atkDamageModifier  = 0.05f;
    public float atkRangeModifier   = 0.01f;
    public float atkSpeedModifier   = 0.01f;
    //public float critDmgModifier = 0.0f;
    // TODO: Add gold income modifier?

    public float upgradeIncreaseCost = 0.2f;
}
