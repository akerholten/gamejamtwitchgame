﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public enum CharacterType
{
    swordsman,
    rifleman
}

public enum Team
{
    blue,
    red
}

public class Character : MonoBehaviour {

    public Team team;
    public CharacterType character; // Should probably make this into an own scriptable object to easily account for different attack modifiers
    public string characterName;       // that depend on character type.

    //public LayerMask blueTeam;
    //public LayerMask redTeam;
    [SerializeField] private LayerMask enemyLayer;
    [SerializeField] private LayerMask friendlyLayer;

    [SerializeField] private Class characterClass;

    // Change these into class values and take into account player's upgraded modifiers
    public float maxHealth = 50.0f;
    public float health = 50.0f;
    public float atkDamage = 5.0f;
    public float atkRange = 0.5f;
    public float atkSpeed = 1.0f;
    public float attackCooldown = 0.0f;
    public float moveSpeed = 1.0f;

    private Rigidbody2D rb;
    private CharacterAnimator animator;
    private Slider healthBar;
    private Vector2 moveDirection;
    private float directionOffset;

    private Random rand;

	void Start () {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<CharacterAnimator>();
        healthBar = transform.Find("Canvas").Find("Healthbar").GetComponent<Slider>();
        healthBar.maxValue = maxHealth;
        healthBar.value = health;
        //InitCharacter(CharacterType.swordsman, "aker");
        if (team == Team.blue)
        {
            moveDirection = Vector2.right;
            directionOffset = 0.1f; // These are for collider checkbox for friendlies
        }
        else
        {
            moveDirection = Vector2.left;
            directionOffset = -0.1f; // These are for collider checkbox for friendlies
        }
        rand = new Random(Time.frameCount);
    }
	
	void Update () {
        attackCooldown -= Time.deltaTime;
        Character enemyTarget = GetEnemy();
        if (enemyTarget != null) // If can't attack
        {
            if (TryAttackEnemy(enemyTarget))
            {
                animator.UpdatePlayerState(PlayerState.attack);
            }
            else
            {
                animator.UpdatePlayerState(PlayerState.idle);
            }
        }
        else
        {
            if (MoveForward())
            {
                animator.UpdatePlayerState(PlayerState.walking);
            }
            else
            {
                animator.UpdatePlayerState(PlayerState.idle);
            }
        }
	}

    public void InitCharacter(string name, Player player, Shop shop) // This could also take in the modifiers from player
    {
        characterName = name;
        TextMesh usernameObject = GetComponentInChildren<TextMesh>();
        usernameObject.text = characterName;

        maxHealth = health = characterClass.health * (1.0f + (player.healthLvl * shop.healthModifier));
        atkDamage = characterClass.atkDamage * (1.0f + (player.atkDamageLvl * shop.atkDamageModifier));
        atkRange = characterClass.atkRange * (1.0f + (player.atkRangeLvl * shop.atkRangeModifier));
        atkSpeed = characterClass.atkSpeed * (1.0f - (player.atkSpeedLvl * shop.atkSpeedModifier));
        moveSpeed = characterClass.moveSpeed;
}

    private Character GetEnemy()
    {
        Collider2D[] enemies = Physics2D.OverlapBoxAll(transform.position, new Vector2(atkRange, atkRange), 0, enemyLayer);
        float bestRange = 9999.9f;
        Collider2D bestEnemy = null;
        foreach (Collider2D enemy in enemies)
        {
            float range = (transform.position - enemy.transform.position).magnitude;
            if (range < bestRange)
            {
                bestRange = range;
                bestEnemy = enemy;
            }
        }
        if (bestEnemy)
        {
            Character enemyCharacter = bestEnemy.GetComponent<Character>();
            if (enemyCharacter) return enemyCharacter; // Found enemy
        }
        return null; // Didn't find enemy
    }

    private bool TryAttackEnemy(Character enemy)
    {
        //attackCooldown -= Time.deltaTime; Move this to update loop to always calculate down attack cd to prevent weird animations and actions?
        if (attackCooldown <= 0.0f)
        {
            if (rand.Next() % 10 == 0) // Critical hit - could do a crit chance modifier here with % 100 - critChance
            {
                enemy.health -= atkDamage * 2;
            }
            else
            {
                enemy.health -= atkDamage; // Possibly add in chance to roll a critical hit?

            }
            enemy.DamageTaken(); // Weird to call here, but should work for now
            if (enemy.health <= 0.0f)
            {
                ArenaManager arenaManager = FindObjectOfType<ArenaManager>();
                arenaManager.PlayerKilled(enemy);
                arenaManager.AwardGold(characterName, arenaManager.killReward);
                Destroy(enemy.gameObject); // Character got killed, reward the Character that got the kill?
            }
            attackCooldown = atkSpeed;
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool MoveForward()
    {
        // Check so you don't collide with a friendly in front of you
        // Arbitrary vector 2 size in this check: (0.8f, 1.0f)
        Collider2D[] friendlies = Physics2D.OverlapBoxAll(new Vector2(transform.position.x + directionOffset, transform.position.y), new Vector2(0.8f, 1.0f), 0, friendlyLayer);
        foreach (Collider2D friendly in friendlies)
        {
            if(friendly != null)
            {
                if (friendly.gameObject == gameObject) { } // To not check itself
                else return false;
            }
        }

        rb.velocity = (moveDirection * moveSpeed);
        return true;
        //Debug.Log("Moved forward");
    }

    public void DamageTaken() // Could possibly make this into OnDamageTaken, and deal with everything related to character dead, UI, etc here
    {
        healthBar.value = health;
    }
}
