﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;


// This class will be managing the arena, reseting, respawning, dealing with Characters attending from twitch, etc..
public class ArenaManager : MonoBehaviour {

    [SerializeField] private GameObject redTeamSwordsman;
    [SerializeField] private GameObject blueTeamSwordsman;
    [SerializeField] private Vector3 redTeamSpawnPos;
    [SerializeField] private Vector3 blueTeamSpawnPos;

    private List<Player> PlayersInQueue;
    private List<Player> blueTeam;
    private List<Player> redTeam;
    private int blueTeamCounter;
    private int redTeamCounter;
    [SerializeField] private float spawnCooldown = 0.5f;
    private float spawnTimer = 0.0f;

    private TwitchClient twitchClient;
    private UIManager userInterfaceManager;
    [SerializeField] private Shop shop;

    [SerializeField] private float roundCooldown;
    private float nextRoundTimer;
    private bool cooldownStarted;
    private bool roundStarted;
    private string winMessage;

    // TODO: Make this later to prevent the bot spamming
    //private string goldMessage; 
    //private bool askedForGold;

    private Random rand;

    public int goldIncome = 5;
    public int killReward = 100;
    public int winReward = 500;
    public float goldIncomeCooldown = 5.0f;
    private float goldIncomeTimer;


    void Start () {
        nextRoundTimer = roundCooldown;
        twitchClient = GetComponent<TwitchClient>();
        cooldownStarted = roundStarted = false;
        blueTeamCounter = redTeamCounter = 0;
        rand = new Random();

        PlayersInQueue  = new List<Player>();
        blueTeam        = new List<Player>();
        redTeam         = new List<Player>();
        DebugPlayersToQueue(); // To Debug to add more players

        userInterfaceManager = FindObjectOfType<UIManager>();
        winMessage = "";
        goldIncomeTimer = goldIncomeCooldown;
    }

    void Update () {
        if (!roundStarted)
        {
            if (!cooldownStarted && twitchClient.IsConnected())
            {
                twitchClient.WriteMessage("Suit up everyone! Next arena round is starting soon, type !enter to queue up!");
                cooldownStarted = true;
                nextRoundTimer = roundCooldown;
            }
            else if (cooldownStarted)
            {
                nextRoundTimer -= Time.deltaTime;
                userInterfaceManager.UpdateStatusText(winMessage + "Next round is starting in " +
                                     ((int)nextRoundTimer + 1).ToString() + " seconds\nType !enter to join.");
                if (nextRoundTimer <= 0.0f)
                {
                    StartRound();
                    roundStarted = true;
                    userInterfaceManager.UpdateStatusText("");
                }
            }
        }
        else
        {
            spawnTimer -= Time.deltaTime;
            if(spawnTimer <= 0.0f)
            {
                if(rand.Next() % 2 == 0) // To somewhat mitigate RNG in terms of who spawns first?
                {
                    if (blueTeamCounter < blueTeam.Count)
                    {
                        SpawnCharacter(blueTeam[blueTeamCounter], Team.blue);
                        blueTeamCounter++;
                    }
                    if (redTeamCounter < redTeam.Count)
                    {
                        SpawnCharacter(redTeam[redTeamCounter], Team.red);
                        redTeamCounter++;
                    }
                }
                else
                {
                    if (redTeamCounter < redTeam.Count)
                    {
                        SpawnCharacter(redTeam[redTeamCounter], Team.red);
                        redTeamCounter++;
                    }
                    if (blueTeamCounter < blueTeam.Count)
                    {
                        SpawnCharacter(blueTeam[blueTeamCounter], Team.blue);
                        blueTeamCounter++;
                    }
                }
                spawnTimer = spawnCooldown;
            }
            if (IsGameOver())
            {
                roundStarted = false;
                cooldownStarted = false;
            }
        }
    }

    private void FixedUpdate()
    {
        goldIncomeTimer -= Time.fixedDeltaTime;
        if (goldIncomeTimer <= 0.0f)
        {
            foreach(Player p in PlayersInQueue)
            {
                p.gold += goldIncome;
            }
            goldIncomeTimer = goldIncomeCooldown;
        }
    }

    public void HandleTwitchInput(string user, string command, MessageType messageType)
    {
        if (command == "!enter")
        {
            Player newPlayer = ScriptableObject.CreateInstance<Player>();
            newPlayer.userName = user;
            if (PlayersInQueue.Exists(player => player.userName == user)) return;
            else PlayersInQueue.Add(newPlayer);
            string response = user + " has joined the arena queue!";
            SendResponse(user, response, messageType);
            userInterfaceManager.UpdateQueueList(user);
        }
        else if(command == "!gold")
        {
            Player currentPlayer = PlayersInQueue.Find(player => player.userName == user);
            if (currentPlayer != null)
            {
                string response = user + " currently has " + currentPlayer.gold + " gold.";
                SendResponse(user, response, messageType);
            }
        }
        else if (command == "!help")
        {
            string response = "Type !enter to join the arena queue, you will join the next available round. For playing you get " +
                              "!gold that you can use to upgrade your character. Write !shop to get the availabe upgrades";
            SendResponse(user, response, messageType);
        }
        else if (command == "!shop")
        {
            // Should probably make a function so upgrades have lvl's and a cap, and also so cost's increase over time
            string response = "To upgrade you can use !upgrade <modifier>. Any modifier will cost a certain amount of gold. Available modifiers (and inital costs) are: " +
                              "attackspeed (150g), health (100g), attackrange (250g), and attackdamage (500g)";
            SendResponse(user, response, messageType);
        }
        else if (command == "!upgrade" || command == "!upgrades")
        {
            // Should probably make a function so upgrades have lvl's and a cap, and also so cost's increase over time
            string response = "To upgrade you can use !upgrade <modifier>. Any modifier will cost a certain amount of gold. Available modifiers (and inital costs) are: " +
                              "attackspeed (150g), health (100g), attackrange (250g), and attackdamage (500g) (Example: '!upgrade health')";
            SendResponse(user, response, MessageType.Whisper);
        }
        // TODO: This can be cleaned alot more if using a struct or scriptable object as upgrades
        else if (command == "!upgrade attackspeed")
        {
            Player player = PlayersInQueue.Find(p => p.userName == user);
            string response = "";
            if (player != null)
            {
                if(player.atkSpeedLvl >= shop.atkSpeedMaxLvl)
                {
                    response = "Your attackspeed is already at max level! (" + shop.atkSpeedMaxLvl + ")";
                    SendResponse(user, response, MessageType.Whisper);
                    return;
                }
                int goldCost = CalculateUpgradeCost(shop.atkSpeedModifierCost, player.atkSpeedLvl);
                if(goldCost <= player.gold)
                {
                    player.gold -= goldCost;
                    player.atkSpeedLvl++;
                    response = "Attackspeed upgraded, current level is now: " + player.atkSpeedLvl + " (Upgrade costed " + goldCost + " gold)";
                }
                else
                {
                    response = "You can not afford that yet, attackspeed upgrade costs: " + goldCost + " gold. (You currently have " + player.gold + ")";
                }
                SendResponse(user, response, MessageType.Whisper);
            }
        }
        else if (command == "!upgrade health")
        {
            Player player = PlayersInQueue.Find(p => p.userName == user);
            string response = "";
            if (player != null)
            {
                if (player.healthLvl >= shop.healthMaxLvl)
                {
                    response = "Your health is already at max level! (" + shop.healthMaxLvl + ")";
                    SendResponse(user, response, MessageType.Whisper);
                    return;
                }
                int goldCost = CalculateUpgradeCost(shop.healthModifierCost, player.healthLvl);
                if (goldCost <= player.gold)
                {
                    player.gold -= goldCost;
                    player.healthLvl++;
                    response = "Health upgraded, current level is now: " + player.healthLvl + " (Upgrade costed " + goldCost + " gold)";
                }
                else
                {
                    response = "You can not afford that yet, health upgrade costs: " + goldCost + " gold. (You currently have " + player.gold + ")";
                }
                SendResponse(user, response, MessageType.Whisper);
            }
        }
        else if (command == "!upgrade attackrange")
        {
            Player player = PlayersInQueue.Find(p => p.userName == user);
            string response = "";
            if (player != null)
            {
                if (player.atkRangeLvl >= shop.atkRangeMaxLvl)
                {
                    response = "Your attackrange is already at max level! (" + shop.atkRangeMaxLvl + ")";
                    SendResponse(user, response, MessageType.Whisper);
                    return;
                }
                int goldCost = CalculateUpgradeCost(shop.atkRangeModifierCost, player.atkRangeLvl);
                if (goldCost <= player.gold)
                {
                    player.gold -= goldCost;
                    player.atkRangeLvl++;
                    response = "Attackrange upgraded, current level is now: " + player.atkRangeLvl + " (Upgrade costed " + goldCost + " gold)";
                }
                else
                {
                    response = "You can not afford that yet, attackrange upgrade costs: " + goldCost + " gold. (You currently have " + player.gold + ")";
                }
                SendResponse(user, response, MessageType.Whisper);
            }
        }
        else if (command == "!upgrade attackdamage")
        {
            Player player = PlayersInQueue.Find(p => p.userName == user);
            string response = "";
            if (player != null)
            {
                if (player.atkDamageLvl >= shop.atkDamageMaxLvl)
                {
                    response = "Your attackdamage is already at max level! (" + shop.atkDamageMaxLvl + ")";
                    SendResponse(user, response, MessageType.Whisper);
                    return;
                }
                int goldCost = CalculateUpgradeCost(shop.atkDamageModifierCost, player.atkDamageLvl);
                if (goldCost <= player.gold)
                {
                    player.gold -= goldCost;
                    player.atkDamageLvl++;
                    response = "Attackdamage upgraded, current level is now: " + player.atkDamageLvl + " (Upgrade costed " + goldCost + " gold)";
                }
                else
                {
                    response = "You can not afford that yet, attackdamage upgrade costs: " + goldCost + " gold. (You currently have " + player.gold + ")";
                }
                SendResponse(user, response, MessageType.Whisper);
            }
        }
    }

    private void SendResponse(string user, string response, MessageType messageType)
    {
        if(messageType == MessageType.Chat)
        {
            twitchClient.WriteMessage(response);
        }
        else if(messageType == MessageType.Whisper)
        {
            twitchClient.WriteWhisper(response, user);
        }
    }

    private void StartRound()
    {
        ResetArena();
        foreach(Player player in PlayersInQueue) //TODO: Pick out randomly from the list
        {
            player.currentlyAlive = true;

            if (blueTeam.Count == redTeam.Count)
            {
                // assign to random team
                if(rand.Next() % 2 == 0)
                {
                    blueTeam.Add(player);
                }
                else
                {
                    redTeam.Add(player);
                }
            }
            else if(blueTeam.Count < redTeam.Count)
            {
                blueTeam.Add(player);
            }
            else
            {
                redTeam.Add(player);
            }
        }
    }

    private void ResetArena()
    {
        foreach(Character character in FindObjectsOfType<Character>())
        {
            Destroy(character.gameObject);
        }
        blueTeam.Clear();
        redTeam.Clear();
        blueTeamCounter = redTeamCounter = 0;
    }

    private void AssignPlayerToTeam()
    {

    }

    private void AssignCharacterToQueue()
    {

    }

    private void DebugPlayersToQueue()
    {
        for(int i = 0; i < 5; i++)
        {
            Player newPlayer = ScriptableObject.CreateInstance<Player>();
            newPlayer.userName = i.ToString();
            PlayersInQueue.Add(newPlayer);
        }
    }

    private void SpawnCharacter(Player player, Team assignedTeam)
    {
        if(assignedTeam == Team.blue)
        {
            if(player.currentCharacter == CharacterType.swordsman)
            {
                GameObject newObject = Instantiate(blueTeamSwordsman, blueTeamSpawnPos, Quaternion.identity);
                Character character = newObject.GetComponent<Character>();
                character.InitCharacter(player.userName, player, shop);
                //character.characterName = player.userName;
            }
        }
        else if(assignedTeam == Team.red)
        {
            if (player.currentCharacter == CharacterType.swordsman)
            {
                GameObject newObject = Instantiate(redTeamSwordsman, redTeamSpawnPos, Quaternion.identity);
                Character character = newObject.GetComponent<Character>();
                character.InitCharacter(player.userName, player, shop);
                //character.characterName = player.userName;
            }
        }
    }

    public void PlayerKilled(Character character)
    {
        if(character.team == Team.blue)
        {
            Player player = blueTeam.Find(p => p.userName == character.characterName);
            player.currentlyAlive = false;
            blueTeam.Remove(player); // should maybe not remove but instead update players to be dead as state?
            // Update UI
        }
        else if(character.team == Team.red)
        {
            Player player = redTeam.Find(p => p.userName == character.characterName);
            player.currentlyAlive = false;
            redTeam.Remove(player); // should maybe not remove but instead update players to be dead as state?
            // Update UI
        }
    }

    private bool IsGameOver()
    {
        List<Player> blueTeamLeft = blueTeam.FindAll(p => p.currentlyAlive == true);
        List<Player> redTeamLeft = redTeam.FindAll(p => p.currentlyAlive == true);
        if (blueTeamLeft.Count <= 0)
        {
            // Update UI, blue team won
            winMessage = "Red team won!\n";
            AwardTeam(Team.red, winReward);
            return true;
        }
        else if(redTeamLeft.Count <= 0)
        {
            // Update UI, red team won
            winMessage = "Blue team won!\n";
            AwardTeam(Team.blue, winReward);
            return true;
        }

        return false;
    }

    public void AwardGold(string user, int goldAmount)
    {
        Player currentPlayer = PlayersInQueue.Find(player => player.userName == user);
        if (currentPlayer != null)
        {
            currentPlayer.gold += goldAmount;
        }
    }

    public void AwardGold(Player player, int goldAmount)
    {
        player.gold += goldAmount;
    }

    public void AwardTeam(Team team, int goldAmount)
    {
        if(team == Team.blue)
        {
            foreach(Player p in blueTeam)
            {
                p.gold += goldAmount;
            }
        }
        else if(team == Team.red)
        {
            foreach(Player p in redTeam)
            {
                p.gold += goldAmount;
            }
        }
    }

    private int CalculateUpgradeCost(int initialCost, int currentLevel)
    {
        return initialCost + (int)(initialCost * (currentLevel * shop.upgradeIncreaseCost));
    }
}
