﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Class Type", menuName = "Class Type")]
public class Class : ScriptableObject {


    // TODO: Work with this later
    CharacterType characterType;
    public float health = 50.0f;
    public float atkDamage = 5.0f;
    public float atkRange = 0.5f;
    public float atkSpeed = 1.0f;
    public float attackCooldown = 0.0f;
    public float moveSpeed = 1.0f;

}
