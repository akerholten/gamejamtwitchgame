﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    private Text queueList;
    private Text statusText;

	void Start () {
        queueList = transform.Find("QueueList").GetComponent<Text>();
        statusText = transform.Find("StatusText").GetComponent<Text>();
	}

    public void UpdateQueueList(string text)
    {
        queueList.text += "\n" + text;
    }

    public void UpdateStatusText(string text)
    {
        statusText.text = text;
    }
}
