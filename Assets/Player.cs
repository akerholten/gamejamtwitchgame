﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : ScriptableObject {

    public string userName;
    public int gold = 0; // Possibly for purchases/upgrades, look into later

    public int healthLvl    = 0;
    public int atkSpeedLvl  = 0;
    public int atkRangeLvl  = 0;
    public int atkDamageLvl = 0;
    //public int critLvl = 0;
    // TODO: Add gold income modifier?

    public CharacterType currentCharacter;
    public bool currentlyAlive = true;

    void Start()
    {
        currentCharacter = CharacterType.swordsman;
        currentlyAlive = true;
    }

}
